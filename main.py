import logging
import argparse
from pathlib import Path
from plyer import notification

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run some arbitrary Python!')
    parser.add_argument('--logfile', help='log file dest')
    args = parser.parse_args()

    logfile = Path(args.logfile or 'stdout.log')
    try:
        logfile.touch()
    except:
        raise Exception('invalid log file')

    logging.basicConfig(
        format="%(asctime)s %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%S%z",
        filename=logfile,
        encoding="utf-8",
        level=logging.DEBUG,
    )

    logging.debug("starting")
    try:
        notification.notify(
            title="Message!",
            message="Here is some more detail.",
            app_icon=None,
            timeout=10,
        )
        logging.debug("success")
    except:
        logging.debug("failure")
    finally:
        logging.debug("done")
