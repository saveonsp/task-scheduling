$cwd = (Get-Item $PSScriptRoot).parent.FullName
$exe = Resolve-Path( Join-Path -Path $cwd -ChildPath .\dist\main.exe )
$logfile = Resolve-Path( Join-Path -Path $cwd -ChildPath .\stdout.log )
$taskname = if ($env:TASK_NAME) { $env:TASK_NAME } else { "Task01" };

$A = New-ScheduledTaskAction -Execute $exe -Argument "--logfile $logfile"
$T = New-ScheduledTaskTrigger -Daily -At 9:57:00am
$P = New-ScheduledTaskPrincipal "SAVEONSP\SZagrobelny"
$S = New-ScheduledTaskSettingsSet
$D = New-ScheduledTask -Action $A -Principal $P -Trigger $T -Settings $S

Register-ScheduledTask -TaskName $taskname -InputObject $D
